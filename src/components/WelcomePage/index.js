import './style.css';

const WelcomePage = ({ user, setIsLoggedIn }) => {
    const HandleLogout = () => {
        setIsLoggedIn(false);
    }

    return (
        <>
            <p>Bem vindo, {user}!</p>
            <button onClick={HandleLogout}>Sair</button>
        </>
    );
}

export default WelcomePage;